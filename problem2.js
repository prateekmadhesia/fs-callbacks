const fs = require("fs");
const path = require("path");

function deleteFiles(cb) {
  let filenamesPath = path.join(__dirname, "/filename.txt");
  const errors = [];
  const success = [];
  let executeCount = 0;

  fs.readFile(filenamesPath, "utf-8", (error, data) => {
    if (error) {
      cb(error);
    } else {
      let newData = data.split(" ");
      const setData = new Set(newData);
      if (setData.has("")) {
        setData.delete("");
      }
      for (let fileName of setData) {
        if (fileName != "") {
          let filePath = path.join(__dirname, fileName);

          setTimeout(() => {
            fs.exists(filePath, (exist) => {
              if (exist) {
                fs.unlink(filePath, (err) => {
                  executeCount += 1;

                  if (err) {
                    errors.push(err);
                  } else {
                    success.push(filePath);
                  }
                  if (setData.size == executeCount) {
                    cb(errors, success);
                  }
                });
              }
            });
          }, 2 * 1000);
        }
      }
    }
  });
}

function sortAndNewFile(cb) {
  let filename = "sorted.txt";
  let filenamesPath = path.join(__dirname, "/filename.txt");
  let lowerCasePath = path.join(__dirname, "/lowerCase.txt");
  let sortedPath = path.join(__dirname, filename);

  fs.readFile(lowerCasePath, "utf-8", (error, data) => {
    if (error) {
      cb(error);
    } else {
      let newData = data
        .split("\n")
        .sort()
        .reduce((accumulator, current) => {
          if (current != "") {
            accumulator += current + "\n";
          }
          return accumulator;
        }, "");

      fs.writeFile(sortedPath, newData, "utf8", (error) => {
        if (error) {
          cb(error);
        } else {
          fs.appendFile(filenamesPath, `${filename} `, "utf8", (err) => {
            if (err) {
              cb(err);
            } else {
              cb(null);
            }
          });
        }
      });
    }
  });
}

function lowerCaseAndNewFile(cb) {
  let filename = "lowerCase.txt";
  let filenamesPath = path.join(__dirname, "filename.txt");
  let upperCasePath = path.join(__dirname, "upperCase.txt");
  let lowerCasePath = path.join(__dirname, filename);

  fs.readFile(upperCasePath, "utf-8", (error, data) => {
    if (error) {
      cb(error);
    } else {
      let newData = data
        .toLowerCase()
        .split(". ")
        .reduce((accumulator, current) => {
          accumulator += current + ".\n";
          return accumulator;
        }, "");

      fs.writeFile(lowerCasePath, newData, "utf8", (err) => {
        if (err) {
          cb(err);
        } else {
          fs.appendFile(filenamesPath, `${filename} `, "utf8", (err) => {
            if (err) {
              cb(err);
            } else {
              cb(null);
            }
          });
        }
      });
    }
  });
}

function upperCaseAndNewFile(data, cb) {
  let filename = "upperCase.txt";
  let filenamesPath = path.join(__dirname, "filename.txt");
  let upperCasePath = path.join(__dirname, filename);

  fs.writeFile(upperCasePath, data.toUpperCase(), (err) => {
    if (err) {
      cb(err);
    } else {
      fs.appendFile(filenamesPath, `${filename} `, "utf8", (err) => {
        if (err) {
          cb(err);
        } else {
          cb(null);
        }
      });
    }
  });
}

function readFile(cb) {
  fs.readFile(path.join(__dirname, "data/lipsum.txt"), "utf-8", (err, data) => {
    if (err) {
      cb(err);
    } else {
      cb(null, data);
    }
  });
}

module.exports = {
  readFile,
  upperCaseAndNewFile,
  lowerCaseAndNewFile,
  sortAndNewFile,
  deleteFiles,
};
