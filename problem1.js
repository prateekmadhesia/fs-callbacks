const fs = require("fs");
const path = require("path");

function deleteFile(filesName, cb) {
  const errors = [];
  const success = [];
  let executeCount = 0;

  filesName.map((file) => {
    setTimeout(()=>{
      let filePath = path.join(__dirname, "output", file);
      

      fs.unlink(filePath, (error) => {
        executeCount+=1;
        if (error) {
          errors.push(error);
        } else {
          success.push(filePath);
        }
        if(file.length == executeCount){
          cb(errors, success);
        }
      });
    }, 2 * 1000);
  });
}

function createFiles(cb) {
  fs.mkdir(path.join(__dirname, "output"), { recursive: true }, (err) => {
    if (err) {
      cb(err);
    } else {
      const filesName = [];
      const jsonToWrite = { Key: "Value" };
      const errors = [];
      const success = [];
      let executeCount = 0;

      for (let index = 1; index <= 10; index++) {
        let name = Math.random().toString(36).substring(2, 7) + ".json";
        filesName.push(name);
      }

      filesName.map((file) => {
        let filePath = path.join(__dirname, "output", file);
        fs.writeFile(
          filePath,
          JSON.stringify(jsonToWrite),
          "utf-8",
          (error) => {
            executeCount += 1;
            if (error) {
              errors.push(error);
            } else {
              success.push(`${filePath}`);
            }
            if (executeCount == 10) {
              cb(errors, success, filesName);
            }
          }
        );
      });
    }
  });
}

module.exports = { createFiles, deleteFile };
