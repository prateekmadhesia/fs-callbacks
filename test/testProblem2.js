const problem2 = require("../problem2.js");

problem2.readFile((err, data) => {
  if (err) {
    console.log("Error on reading file");
    console.error(err);
  } else {
    problem2.upperCaseAndNewFile(data, (upperError) => {
      if (upperError) {
        console.error(
          "Error on writing upperCase.txt and appending names in filenames.txt"
        );
        console.error(upperError);
      } else {
        problem2.lowerCaseAndNewFile((lowerError) => {
          if (lowerError) {
            console.error(
              "Error on writing lowerCase.txt and appending names in filenames.txt"
            );
            console.error(lowerError);
          } else {
            problem2.sortAndNewFile((sortError) => {
              if (sortError) {
                console.error(
                  "Error on writing sorted.txt and appending names in filenames.txt"
                );
                console.error(sortError);
              } else {
                problem2.deleteFiles((deleteError, deleteSuccess) => {
                  if (deleteError.length > 0) {
                    console.error("Error on delete file");
                    console.error(deleteError);
                    console.log("Success on delete file");
                    console.log(deleteSuccess);
                  } else {
                    console.log("Success on delete file");
                    console.log(deleteSuccess);
                  }
                });
              }
            });
          }
        });
      }
    });
  }
});
