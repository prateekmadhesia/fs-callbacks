const problem1 = require("../problem1.js");

problem1.createFiles((error, success, filesName) => {
  if (error.length > 0) {
    console.log("Error on deleted");
    console.error(error);
    console.log("Successfully created");
    console.log(success);
  } else {
    console.log("Successfully created");
    console.log(success);
    problem1.deleteFile(filesName, (err, success) => {
      if (err.length > 0) {
        console.log("Error on deletion");
        console.error(err);
        console.log("Successfully deleted");
        console.error(success);
      } else {
        console.log("Successfully deleted");
        console.error(success);
      }
    });
  }
});
